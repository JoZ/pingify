# pingify
pingify is a shell script that monitors if a set of hosts is up.
It pings them at a configurable interval, and documents state changes.

## Example setup
pingify.service lies in /etc/systemd/system.

Content of `/etc/pingify/conf`:
```
INTERVAL=2
HOSTSFILE=/etc/pingify/hosts
```
/etc/pingify/hosts:
```
# Lines starting with '#' are ignored
google.com
google.fi
# ↓ Empty lines are also ignored

wikipedia.org
myhomeserver.local
```
`systemctl enable --now pingify`

`journalctl -fu pingify`

Now unplug your network cable. You will see state changes.